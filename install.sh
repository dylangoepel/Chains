#!/usr/bin/env bash

# Welcome text
cat motd

# Get version (last commit count)
version=`echo "scale=2; $(git rev-list --count HEAD) / 100" | bc`
echo $version > version
echo Installing CHAINS v$version

sudo tools/chpac .

sudo cp version /usr/share/chains/version
sudo cp motd /usr/share/chains/motd
figlet Alpha `cat version` | sudo tee -a /usr/share/chains/motd 
