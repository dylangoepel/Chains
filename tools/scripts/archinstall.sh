pacman -Syu --noconfirm
pacman -S --noconfirm python python-pip gcc git
pip3 install pycrypto
tools/buildlib ./python
gcc cli/daemon/main.c -o cli/daemon/daemon
sudo gcc /usr/share/chains/cli/daemon/cbdp.c -o /usr/share/chains/cli/daemon/cbdp
ln -s cli/chains.py /bin/chains
