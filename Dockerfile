FROM base/archlinux
ADD . /usr/share/chains
WORKDIR /usr/share/chains
RUN bash tools/archinstall.sh
CMD /bin/bash
