from json import loads, dumps
from json.decoder import JSONDecodeError
import Chains.blockchain as bc


class ObjectValue(dict):
    ''' dict with json str converter '''
    def __str__(self):
        return dumps(self)

class ObjectBody(bc.Body):
    ''' ObjectBlock content '''
    def __init__(self, blocktype, params):
        ''' Generate ObjectBlock content of type blocktype and parameters params '''
        self.value = ObjectValue(params)
        self.blocktype = blocktype
        self.value['type'] = str(self.blocktype)

    def __str__(self):
        ''' Generate JSON ObjectBlock body '''
        return dumps(self.value)

    def fromString(jstring):
        value = loads(jstring)
        btype = value['type']
        return ObjectBlock(btype, value)

    def hash(self):
        return bc.hash(str(self.value))

    def hasContent(self):
        return True

class ObjectBlock(bc.Block):
    ''' Advanced block with JSON dict as body '''
    def __init__(self, btype, value, last):
        self.body = ObjectBody(btype, value)
        self.head = bc.Head(self.body, last.head)

    def fromBlock(block):
        ''' Get object block from Block '''
        ob = _ObjectBlock()
        ob.head = block.head
        params = loads(block.body.value)
        ob.body = ObjectBody(params['type'], params, )
        return ob

    def fromString(blockstr):
        ''' Get ObjectBlock from Block summary '''
        self = ObjectBlock.fromBlock(bc.Block.fromString(blockstr))
        return self

class _ObjectBlock(ObjectBlock):
    def __init__(self):
        pass

class ObjectBlocks(bc.Blocks):
    ''' list of ObjectBlock objects with additional features '''
    def __init__(self, *args, **kwargs):
        bc.Blocks.__init__(self, *args, **kwargs)
        self.blocktypes.reverse()
        self.blocktypes.append(ObjectBlock)
        self.blocktypes.append(_ObjectBlock)
        self.blocktypes.reverse()

    def getTypes(self):
        ''' Get all existent ObjectBlock 'type' attributes in block list '''
        types = []
        for block in self:
            try:
                if not block.body.value['type'] in types:
                    types.append(block.body.value['type'])
            except TypeError:
                continue
        return types

    def getType(self, btype):
        ''' get all blocks with 'type' attribute equivalent to btype '''
        retblocks = []
        for block in self:
            try:
                if block.body.value['type'] == btype:
                    retblocks.append(block)
            except TypeError:
                pass
        return retblocks

    def fromLines(lines, last, **kwargs):
        self = ObjectBlocks(**kwargs)
        return self._fromLines(lines, last, **kwargs)

class Blockchain(bc.Blockchain):
    ''' Simple class to extract or append blocks from / to blockchain '''
    def __init__(self, path):
        self.path = path

    def getTypes(self):
        ''' Get different type attributes of all ObjectBlocks '''
        blocks = self.getBlocks()
        return blocks.getTypes()

    def getBlocks(self):
        ''' Get Blocks Object with all blocks in the blockchain '''
        with open(self.path) as f:
            lines = f.readlines()
        blocks = ObjectBlocks.fromLines(lines, bc.GenesisBlock())
        return blocks

    def appendObjectBlock(self, oblock):
        ''' Append an object block class to the blockchain file '''
        self.append(str(oblock))

    def getLastBlock(self):
        try:
            with open(self.path) as f:
                lines = f.readlines()
            try:
                lastline = lines[len(lines) - 1].replace("\n", "")
            except IndexError:
                Blockchain(self.path)
                return bc.GenesisBlock()
            lastblock = bc.Block.fromString(lastline)
        except FileNotFoundError:
            bc.Blockchain(self.path)
            lastblock = bc.GenesisBlock()
        return lastblock


    def appendObject(self, btype, params):
        ''' Generate and append object block '''
        lastblock = self.getLastBlock()
        oblock = ObjectBlock(btype, params, lastblock)
        with open(self.path, "a") as f:
            f.write(str(oblock) + "\n")

    def append(self, *args):
        self.appendObject(*args)
        lastblock = self.getLastBlock()
