from sys import argv, exit
import Chains

try:
    from Chains import crypto
    cryptoimp = True
except:
    cryptoimp = False

def getKeyFile():
    return argv[1]

def getBlockchainFile():
    return argv[2]

def getArgv():
    try:
        return [argv[0], *argv[3:]]
    except KeyError:
        return []

def getKey():
    if not cryptoimp:
        raise ImportError
    return crypto.Key(getKeyFile())

def getBlockchain():
    return Chains.Blockchain(getBlockchainFile())
