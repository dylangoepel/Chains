from hashlib import sha512 as hashfunc
from base64 import b64encode as encode, b64decode as decode

from os.path import isfile
from os import remove

from .exception import ChainsError, BlockchainError
from .exception import ChainsException, CryptoError

genesisvalue = "GENESIS"

def hash(data):
    ''' Hash UTF-8 encoded string from data '''
    if type(data) == bytes:
        return hashfunc(data).hexdigest()
    else:
        return hashfunc(str(data).encode("utf-8")).hexdigest()

class GenesisHead:
    def __init__(self, body):
        ''' Create GenesisBlock head with hash from Body() object '''
        self.hash = hash(body.hash())

class Head:
    ''' Block Hash '''
    def __init__(self, body, last):
        ''' Create Block head with hash from Body() object, body, and Head() object, last '''
        self.hash = hash(body.hash() + last.hash)


class Body:
    ''' Block content '''
    def __init__(self, value):
        self.value = str(value)

    def hash(self):
        ''' Get hash from body value '''
        return hash(self.value)

class GenesisBlock:
    ''' Minimal GenesisBlock class with hashing features '''
    def __init__(self):
        ''' Create GenesisBlock with Body() and Head() objects. '''
        self.body = Body(str(genesisvalue))
        self.head = GenesisHead(self.body)

    def __str__(self):
        ''' Get block summary in str() format and the hash and the b64 encoded value separated by a space '''
        return " ".join([self.head.hash,
                        encode(str(self.body.value).encode(
                        "utf-8")).decode("utf-8")])

    def check(self):
        ''' Check the GenesisHead hash '''
        checker = GenesisHead(self.body)
        if checker.hash != self.head.hash:
            return False
        return True

    def fromString(summary):
        ''' Parse str() format block summary generated with GenesisBlock.__str__'''
        block = GenesisBlock()
        items = summary.split(" ")
        if len(summary.split()) != 2:
            raise BlockchainError("Invalid block line syntax")
        try:
            block.body.value = decode(items[1].encode()).decode()
        except IndexError:
            block.body.value = ""
        except:
            raise BlockchainError("Block Value not base64 encoded")
        block.head.hash = items[0]
        if str(block.body.value) != genesisvalue:
            raise ValueError("Not a genesis block")
        return block

class Block(GenesisBlock):
    ''' Minimal block class with hashing features '''
    def __init__(self, value, last):
        self.body = Body(str(value))
        self.head = Head(self.body, last.head)

    def fromString(summary):
        ''' Parse str() format block summary generated with Block.__str__'''
        block = GenesisBlock()
        items = summary.split(" ")
        if len(summary.split()) != 2:
            raise BlockchainError("Invalid block line syntax")
        try:
            block.body.value = decode(items[1].encode()).decode()
        except IndexError:
            block.body.value = ""
        except:
            raise BlockchainError("Block Value not base64 encoded")
        block.head.hash = items[0]
        return Block.fromGenesisBlock(block)

    def fromGenesisBlock(gBlock):
        ''' Create block from GenesisBlock gBlock '''
        b = Block("", GenesisBlock())
        b.body = gBlock.body
        b.head.hash = gBlock.head.hash
        return b

    def toGenesisBlock(self):
        ''' Create GenesisBlock from self '''
        b = GenesisBlock()
        b.head.hash = self.head.hash
        return b

    def check(self, last):
        ''' Check block hash '''
        if self.body.value == "":
            return True
        checker = Head(self.body, last.head)
        if not checker.hash == self.head.hash:
            return False
        return True

class Blocks(list):
    ''' Block list with additional features '''
    def __init__(self, *args, **kwargs):
        list.__init__(self, *args, **kwargs)
        self.blocktypes = [GenesisBlock, Block]

    def check(self, *args):
        ''' Check all blocks in the list '''
        if len(args) > 1:
            raise ValueError()
        if len(args) == 0:
            lb = 0
        else:
            lb = args[0]
        for block in self:
            if type(block) == GenesisBlock:
                if not block.check():
                    return False
            else:
                if not block.check(lb):
                    return False
            lb = block
        return True

    def _fromLines(self, lines, last, **kwargs):
        for line in lines:
            block = False
            for blocktype in self.blocktypes:
                try:
                    block = blocktype.fromString(line)
                    break
                except:
                    pass
            if block:
                self.append(block)
            else:
                raise ValueError("Unable to parse block")
            last = block
        return self

    def fromLines(lines, last, **kwargs):
        self = Blocks(**kwargs)
        return self._fromLines(lines, last, **kwargs)

class Blockchain:
    def __init__(self, path):
        self.path = path
        if not isfile(path):
            gen = GenesisBlock()
            with open(path, "w") as f:
                f.write(str(gen) + "\n")
                f.close()

    def new(path):
        return Blockchain(path)


    def append(self, value):
        if not isfile(self.path):
            self.create()
            gen = GenesisBlock()
            with open(self.path, "w") as f:
                f.write(str(gen) + "\n")
                f.close()
        n = self.countLines()
        prev = self.getLine(n)
        if not prev:
            return False
        try:
            block = Block.fromString(prev)
        except BlockchainError:
            raise BlockchainError
        nblock = Block(value, block)
        with open(self.path, "a+") as f:
            f.write(str(nblock) + "\n")
            f.close()
        return n + 1


    def countLines(self):
        if not isfile(self.path):
            return 0
        with open(self.path, "r") as f:
            i = int(0)
            ln = f.readline()
            while ln:
                if ln != "":
                    i += 1
                ln = f.readline()

            f.close()
        return i

    def getBlock(self, num):
        line = self.getLine(num)
        if not line:
            return False
        b = Block.fromString(line)
        return b

    def getLine(self, num):
        if not isfile(self.path):
            raise BlockchainError(self.path + " is not a file")
        ret = False
        with open(self.path, "r") as f:
            n = int(0)
            data = f.readline()
            while data:
                n += 1
                if n == num:
                    ret = data
                    break
                data = f.readline()
            f.close()
        return ret

    def create(self):
        with open(self.path, "w") as f:
            f.close()

    def checkBlock(self, n):
        try:
            bl = self.getBlock(n)
        except:
            return False
        return bl.check()

    def check(self, verbose=False):
        maxnum = self.countLines()
        genline = self.getLine(1)
        if not genline:
            raise ValueError("Blockchain is empty")
        genesis = Block.fromString(genline).toGenesisBlock()
        if not genesis.check():
            raise CryptoError("Genesis block is incorrect")
        oldblock = genesis
        i = int(1)
        for line in range(2, self.countLines() + 1):
            i += 1
            parseline = self.getLine(line)
            if not parseline:
                if line == self.countLines():
                    return True
                raise ValueError("Unable to get line #" + str(line))
            block = Block.fromString(parseline)
            if not block.check(oldblock):
                raise CryptoError("Block " + str(i) + " is incorrect")
            oldblock = block

    def changeLine(self, n, new):
        with open(self.path, "r") as f:
            i = 0
            writer = open(self.path + ".tmp", "w")
            for line in f.readlines():
                i += 1
                if i == n:
                    writer.write(new)
                    continue
                writer.write(line)
            writer.close()
        reader = open(self.path + ".tmp", "r")
        writer = open(self.path, "w")
        for line in reader.readlines():
            writer.write(line)
        reader.close()
        writer.close()
        remove(self.path + ".tmp")

    def isNewer(self, path):
        bc = Blockchain(path)
        mylines = self.countLines()
        if self.countLines() < bc.countLines():
            lastline = self.getLine(mylines)
            if lastline != bc.getLine(mylines):
                return False
            try:
                bc.check()
            except CryptoError:
                return False
            except BlockchainError:
                return False
            return True
        else:
            return False

    def hasContent(self, bid):
        try:
            b = self.getBlock(bid)
        except:
            return False
        return True

    def insertBlock(self, bid, block):
        with open(self.path, "r") as f:
            lines = f.readlines()
        lines[bid - 1] = str(block) + "\n"
        with open(self.path, "w") as f:
            f.write("".join(lines))

    def insertContent(self, bid, value, tmpsuffix=".tmp"):
        w = open(self.path + tmpsuffix, "w")
        with open(self.path, "r") as f:
            data = f.readine()
            i = 0
            lb = False
            while data:
                i += 1
                if i != bid:
                    w.write(data)
                    if i == bid - 1:
                        lb = Block.fromString(data)
                else:
                    b = Block.fromString(data)
                    b.body.value = str(value)
                    if not b.check(lb):
                        w.close()
                        remove(self.path + tmpsuffix)
                        return False
                    w.write(str(b) + "\n")
                data = f.readline()

            f.close()

        w.close()
        w = open(self.path, "w")
        with open(self.path + tmpsuffix) as f:
            data = f.readline()
            while data:
                w.write(data)
                data = f.readline()
            f.close()

        remove(self.path + tmpsuffix)
        return True
