class ChainsError(Exception):
    pass

class BlockchainError(ChainsError):
    pass

class ChainsException(ChainsError):
    pass

class CryptoError(ChainsError):
    pass

