# Chains
The blockchain library which allows you decentralize your applications.

## License
This software is licensed under the BSD-2-clause license.
See LICENSE for more information

## Install
### Linux
#### Linux
```
git clone https://gitlab.com/dylangoepel/Chains.git; cd Chains; ./install.sh
```
Now you can execute Chains in a container by running
```
docker run -it chains
```

## Contribute
##### Please mail dylangoepel@t-online.de
